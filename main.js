/******************************************************************************
 * IMPORT
 ******************************************************************************/
import { MOTW } from "./system/scripts/config.js";
import { registerHelpers } from "./system/scripts/handlebars.js";

// Import Models
import { MacroModel } from "./system/models/macro.js";
import { ItemModel } from "./system/models/item.js";
import { ActorModel } from "./system/models/actor.js";


// Import Controllers
import { ItemController } from "./system/controllers/item_controller.js";
import { ImprovementController } from "./system/controllers/improvement_controller.js";
import { ArchetypeController } from "./system/controllers/archetype_controller.js";
import { MoveController } from "./system/controllers/move_controller.js";
import { WeaponController } from "./system/controllers/weapon_controller.js";
import { HunterController } from "./system/controllers/hunter_controller.js";


// Import Helpers

/******************************************************************************
 * HOOKS
 ******************************************************************************/

Hooks.once('init', async function() {

  game.motw = {
    ActorModel,
    MacroModel,
    ItemModel,
  };

  //CONFIG.debug.hooks = true
  CONFIG.MOTW = MOTW;
  CONFIG.Actor.entityClass = ActorModel;
  CONFIG.Item.entityClass = ItemModel;

  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("motw", HunterController, { types: ["hunter", "npc"], makeDefault: true });

  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("motw", ItemController, { types: ["item"], makeDefault: true });
  Items.registerSheet("motw", ImprovementController, { types: ["improvement"], makeDefault: true });
  Items.registerSheet("motw", MoveController, { types: ["move"], makeDefault: true });
  Items.registerSheet("motw", WeaponController, { types: ["weapon"], makeDefault: true });
  Items.registerSheet("motw", ArchetypeController, { types: ["archetype"], makeDefault: true });

  registerHelpers();
});

Hooks.once("setup", function () {
});

Hooks.on("renderHunterController", (...args) => {
  if (args[2].archetype) return;

  document.getElementById(`actor-${args[2].actor._id}`)
          .querySelector(".item[data-tab='career']")
          .click();
});


Hooks.on("ready", async () => {
  CONFIG.ImportCompendium = "";

  if (!CONFIG.ImportCompendium) return;

  // Reference a Compendium pack by it's callection ID
  const pack = game.packs.find(p => p.collection === `world.export`);

  // Load an external JSON data file which contains data for import
  const content = await fetch(`systems/motw/utils/compendium-creation/json/${CONFIG.ImportCompendium}.json`)
                        .then(response => response.json());


  const items = [];

  for (let e of content) {
    const f = await Item.create(e, {temporary: true});
    pack.importEntity(f);
  }
});

Hooks.on("hotbarDrop", (bar, data, slot) => MacroModel.create(data, slot));
