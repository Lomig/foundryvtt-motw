![Monster of the Week Banner](images/banner.jpg "Monster of the Week")
# Monster of the Week for Foundry VTT
This game system for Foundry Virtual Tabletop provides character sheet and game system support for the Monster of the Week roleplaying game.

## About Monster of the Week
[Monster of the Week](https://genericgames.co.nz/motw/) is a standalone action-horror RPG for 3 to 5 people, based on PbtA (Powered by the Apocalypse) system.

Hunt high school beasties à la *Buffy the Vampire Slayer*, travel the country to bring down unnatural creatures like the Winchester brothers of *Supernatural*, or head up the government investigation like Mulder and Scully in *X-Files*, one monster at a time!

## About Foundry VTT
[Foundry VTT](https://foundryvtt.com) is a standalone application built for experiencing multiplayer tabletop RPGs using a feature-rich and modern self-hosted application where your players connect directly through the browser.

## Monster of the Week for Foundry VTT Game System
### Disclaimer
This system is tailored to my Game Master's needs, and some choices may not be on par with your own expectations.
Even though I try to keep others in mind, it is not in the scope of the project at all.

### Installation Instructions
To install and use the Monster of the Week system for Foundry Virtual Tabletop, simply paste the following URL into the  **Install System**  dialog on the Setup menu of the application:

[https://gitlab.com/Lomig/foundryvtt-motw/-/raw/master/system.json](https://gitlab.com/Lomig/foundryvtt-motw/-/raw/master/system.json)

If you wish to manually install the system, you must clone or extract it into the  `Data/systems/motw` folder.

### Localization
The system will ultimately work in English and French. This early in the process, French is the only working language for most parts of the system.

### Community Contribution
At this early stage, any contribution is pointless, as nothing is to be considered as definitive. This message will be updated when the core mechanics of this system are stabilized.

### Screenshots
#### Character Sheet (Work in Progress)
![Character Sheet Screenshot](images/character-sheet.png "Character Sheet")
