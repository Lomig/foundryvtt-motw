import { SelectionController } from "./selection_controller.js";

export class WeaponSelectionController extends SelectionController {


  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      template: "systems/motw/system/views/weapon/selection.html",
      title: game.i18n.localize("MOTW.Weapons")
    });

    return options;
  }

  async getData() {
    const data = super.getData();
    const pack = game.packs.find(p => p.collection === "motw.weapons");
    const content = await pack.getContent();

    data.weapons = content;
    return data;
  }

  onDragStart(event) {
    const li = event.currentTarget.closest(".directory-item");
    const dragData = { type: "Weapon", pack: "motw.weapons", id: li.dataset.entryId, actorTarget: this.actor._id };
    super.onDragStart(event, dragData, li);
  }
}
