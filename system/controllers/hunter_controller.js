import { Message } from "../models/message.js";
import { ImprovementSelectionController } from "./improvement_selection_controller.js";
import { ArchetypeSelectionController } from "./archetype_selection_controller.js";
import { MoveSelectionController } from "./move_selection_controller.js";
import { WeaponSelectionController } from "./weapon_selection_controller.js";


Hooks.once('init', async function() {
 const partials = [
    "systems/motw/system/views/hunter/partials/_headers.html",
    "systems/motw/system/views/hunter/partials/_navbar.html",
    "systems/motw/system/views/hunter/partials/_career_tab.html",
    "systems/motw/system/views/hunter/partials/_basic_moves_tab.html",
    "systems/motw/system/views/hunter/partials/_hunter_moves_tab.html",
    "systems/motw/system/views/hunter/partials/_improvements_tab.html",
    "systems/motw/system/views/hunter/partials/_equipment_tab.html"
  ];

  loadTemplates(partials);
});

export class HunterController extends ActorSheet {


  static get defaultOptions() {
    this.initialTab = this.initialTab || "basic-moves";

    return mergeObject(super.defaultOptions, {
      classes: ["motw", "char-sheet", "hunter-sheet"],
      template: "systems/motw/system/views/hunter/sheet.html",
      width: 600,
      height: 620,
      tabs: [
        {
          navSelector: ".tabs",
          contentSelector: ".tab",
          initial: this.initialTab,
        },
      ],
    });
  }

  getData() {
    const data = super.getData();
    const hunter = this.actor;
    data.strings = {};

    data.archetype = hunter.archetype()?.data;
    data.inventory = hunter.inventory.map(item => item.data);
    data.basicMoves = hunter.basicMoves();
    data.hunterMoves = hunter.hunterMoves();
    data.alienMoves = hunter.alienMoves();

    const improvements = hunter.improvements();
    [data.advancedImprovements, data.basicImprovements] = improvements.reduce((result, improvement) => {
      improvements.isAdvanced ? result[Ø].push(improvement) : result[1].push(improvement);
      return result;
    }, [[], []]);

    data.strings.noArchetype = game.i18n.localize("MOTW.DragNDrop");
    data.strings.ownMoves = game.i18n.format("MOTW.OwnMoves", {archetype: data.archetype?.name});

    const hunterMovesCount = data.hunterMoves.length;
    const maxHunterMoves = hunter.maxHunterMoves();
    let color = hunterMovesCount === maxHunterMoves ? 'green' : 'orange';
    data.strings.hunterMoveCount = `{{{ color '${hunterMovesCount} / ${maxHunterMoves}' '${color}' }}}`;

    const alienMovesCount = data.alienMoves.length;
    const maxAlienMoves = hunter.maxAlienMoves();
    color = alienMovesCount === maxAlienMoves ?  'green' : 'orange';
    data.strings.alienMoveCount = `{{{ color '${alienMovesCount} / ${maxAlienMoves}' '${color}' }}}`;

    const basicImprovementsCount = data.basicImprovements.length;
    const advancedImprovementsCount = data.advancedImprovements.length;
    data.strings.basicImprovementsCount = `{{{ color '${data.basicImprovements.length} / 10' 'green' }}}`;
    color = basicImprovementsCount < 4 && advancedImprovementsCount > 0 ? 'orange' : "green";
    data.strings.advancedImprovementsCount = `{{{ color '${advancedImprovementsCount} / 7' '${color}' }}}`;

    data.weapons = hunter.weapons();
    data.armors = hunter.armors();
    data.inventory = hunter.inventory;

    return data;
  }

  // Override parent class
  _createEditor(target, editorOptions, initialContent) {
    editorOptions.toolbar = "styleselect bullist hr image removeFormat save";
    editorOptions.height = 240;
    super._createEditor(target, editorOptions, initialContent);
  }

  // Override parent class
  async _onResize(event) {
    super._onResize(event);
    let html = $(event.path[2]);
    let editors = html.find(".panel .content");
    editors.css("height", this.position.height - 340);
  }

  // When something is dropped on the sheet
  async _onDrop(event) {
    const {type, pack, id, actorTarget} = JSON.parse(event.dataTransfer.getData("text/plain"));
    const actor = this.actor;
    if (actor._id !== actorTarget) return Message.warn("MOTW.DropFromWrongWindow");

    const collection = game.packs.get(pack);
    const entity = await collection.getEntity(id) || game.items.get(id);
    if (!entity) return Message.warn("MOTW.NoEntityFoundFromDrop");

    if (type === "Archetype") return this.onDropArchetype(actor, entity);
    if (type === "Improvement") return this.onDropImprovement(actor, entity);
    if (type === "Move") return this.onDropMove(actor, entity);
    if (type === "Item") return this.onDropItem(actor, entity);
  }

  onDropImprovement(actor, entity) {
    entity.data.data.date = Date.now();
    this.improvementSelection.close();
    actor.createOwnedItem(entity)
         .then(success => actor.updateAttributes());
    return Message.info("MOTW.CharacterImprovedMessage", {name: actor.name});
  }

  onDropArchetype(actor, entity) {
    this.archetypeSelection.close();
    actor.createOwnedItem(entity)
         .then(success => actor.updateAttributes());
    return Message.info("MOTW.StartingMovesMessage", {count: entity.moves()});
  }

  onDropMove(actor, entity) {
    actor.createOwnedItem(entity)
         .then(itemCreated => this.moveSelection.render());
    return Message.info("MOTW.MoveLearned");
  }

  onDropWeapon(actor, entity) {
    actor.createOwnedItem(entity)
         .then(itemCreated => this.moveSelection.render());
    return Message.info("MOTW.ItemAdded", {name: actor.name});
  }

  onDropItem(actor, entity) {
    return Message.warn("Not implemented yet");
  }

  activateListeners(html) {
    super.activateListeners(html);
    if (!this.options.editable) return;


    // Make every ".drag" element draggable
    if (this.actor.owner) {
      html.find('.drag').each((i, element) => {
        element.setAttribute("draggable", true);
        element.addEventListener("dragstart", (event) => {
          this._onDragStart(event);
        }, false);
      });
    }


    // Roll Attribute on click
    html.find(".attribute label").click((event) => {
      const attribute = event.currentTarget.parentElement.dataset.attributeId;
      this.actor.rollAttribute(attribute);
    });

    // Attribute bonus on click
    html.find(".attribute .bonus").click((event) => {
      const attribute = event.currentTarget.parentElement.parentElement.dataset.attributeId;
      this.actor.toggleBonusAttribute(attribute);
    });


    // Roll Item on click
    html.find(".roll").click((event) => {
      const id = event.currentTarget.parentElement.dataset.itemId;
      const item = this.actor.getOwnedItem(id);
      item.roll();
    });


    // Select input content on click
    html.find("input").click((event) => {
      event.currentTarget.select();
    });


    // Change Resources amounts on click
    html.find(".resource-data").click((event) => {
      const id = event.currentTarget.parentElement.dataset.fieldId;
      const delta = event.currentTarget.classList.contains("active") ? -1 : 1;
      this.actor.updateResources(id, delta);
    });


    // Check / Uncheck Instable checkbox on click
    html.find(".checkbox-data").click((event) => {
      const id = event.currentTarget.parentElement.dataset.fieldId;
      const newData = {};
      const field = `data.${id}`;
      setProperty(newData, field, !getProperty(this.actor.data, field));
      this.actor.update(newData);
    });


    // Select new starting attribute from Archetype on click
    html.find(".archetype-stat").click((event) => {
      const id = parseInt(event.currentTarget.dataset.id, 10);
      const archetype = this.actor.archetype();
      const newData = {_id: archetype.id, data: {selected: id}};
      this.actor.updateOwnedItem(newData)
                .then(success => this.actor.updateAttributes())
    });


    // Update Inventory Item
    html.find(".item-edit").click((event) => {
      const li = $(event.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });


    // Delete Inventory Item
    html.find(".item-delete").click((event) => {
      const li = $(event.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"))
                .then(success => this.moveSelection?.render());
      li.slideUp(200, () => this.render(false));
    });


    // Collapsible Handler
    html.find(".collapsible-header").click((event) => {
      if (event.target.classList.contains("fa-dice")) return;

      this._onCollapsibleClick(event);
    });


    // Open Archetype Compendium
    html.find("#archetype-link").click((event) => {
      this.archetypeSelection = this.archetypeSelection || new ArchetypeSelectionController(this.actor);
      this.archetypeSelection.render(true);
    });


    // Open Improvement Selection
    html.find(".basic-improvements i").click((event) => {
      this.improvementSelection = this.improvementSelection || new ImprovementSelectionController(this.actor);
      this.improvementSelection.render(true);
    });

    // Open Move Selection
    html.find(".own-moves i").click((event) => {
      this.moveSelection = this.moveSelection || new MoveSelectionController(this.actor);
      this.moveSelection.render(true);
    });
  }

  // Collapsible Management
  _onCollapsibleClick(event) {
    event.preventDefault();
    const collapsible = event.currentTarget.parentNode;
    const collapsibleHeader = event.currentTarget;
    const collapsibleContent = collapsible.querySelector(".collapsible-content");
    const icon = collapsibleHeader.querySelector(".collapsible-icon");


    if (collapsibleContent.classList.contains("collapsed")) {
      $(collapsibleContent).slideDown(200, () => {
        collapsibleContent.classList.remove("collapsed");
        icon.classList.remove("fa-angle-down");
        icon.classList.add("fa-angle-up");
      });
    } else {
      $(collapsibleContent).slideUp(200, () => {
        collapsibleContent.classList.add("collapsed");
        icon.classList.remove("fa-angle-up");
        icon.classList.add("fa-angle-down");
      });
    }
  }
}
