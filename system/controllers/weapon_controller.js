export class WeaponController extends ItemSheet {


  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["motw", "sheet", "weapon-sheet"],
      template: "systems/motw/system/views/weapon/sheet.html",
      width: 520,
      height: 480,
    });
  }
}
