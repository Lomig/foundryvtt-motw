export class ImprovementController extends ItemSheet {


  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["motw", "sheet", "improvement-sheet"],
      template: "systems/motw/system/views/improvement/sheet.html",
      resizable: false
    });
  }
}
