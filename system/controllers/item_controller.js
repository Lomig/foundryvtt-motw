export class ItemController extends ItemSheet {


  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["motw", "sheet", "item-sheet"],
      template: "systems/motw/system/views/item/sheet.html",
      width: 520,
      height: 480,
    });
  }
}
