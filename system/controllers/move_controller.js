export class MoveController extends ItemSheet {


  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["motw", "sheet", "move-sheet"],
      template: "systems/motw/system/views/move/sheet.html",
      width: 600,
      height: 331,
      resizable: false
    });
  }
}
