export const MoveModel = {


  archetype() {
    const move = this.data
    return move.data.archetype;
  },

  isBasicMove() {
    const move = this.data
    return this.archetype().length === 0;
  },

  async roll() {
    const move = this.data;
    if (!move.data.attribute) return this.warn("MOTW.ItemDontNeedRoll", {item: this.data.name});

    const token = this.actor.token;
    const actorData = this.actor ? this.actor.data.data : {};

    const roll = new Roll(`2d6+@attributes.${move.data.attribute}.value+@attributes.${move.data.attribute}.mod`, actorData);
    const label = move.name;

    roll.roll().toMessage({
      speaker: ChatMessage.getSpeaker({ actor: this.actor }),
      flavor: label
    });

    return roll;
  },
}
