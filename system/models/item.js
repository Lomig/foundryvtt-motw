import { Message } from "./message.js";

import { ArchetypeModel } from "./itemModels/archetype.js"
import { ImprovementModel } from "./itemModels/improvement.js"
import { MoveModel } from "./itemModels/move.js"
import { WeaponModel } from "./itemModels/weapon.js"

export class ItemModel extends Item {
  constructor(...args) {
    super(...args);

    if (this.type === "archetype") {
      Object.assign(this, ArchetypeModel);
    } else if (this.type === "move") {
      Object.assign(this, MoveModel);
    } else if (this.type === "improvement") {
      Object.assign(this, ImprovementModel);
    } else if (this.type === "weapon") {
      Object.assign(this, WeaponModel);
    }
  }

  get codeName() {
    const item = this.data
    return item.data.code || undefined;
  }

  get description() {
    const item = this.data
    return item.data.description || undefined;
  }

  roll() {
    return Message.warn("MOTW.CantRollItem", {item: this.data.name});
  }
}
