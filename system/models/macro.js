import { Message } from "./message.js";

export class MacroModel {


  static async create(data, slot) {
    if (data.type !== "Item") return;
    if (!("data" in data)) return Message.warn("MOTW.MacroForOwnedItemsOnly");

    const item = data.data;
    const command = `game.motw.MacroModel.roll("${item.name}");`;
    const macro = this.find(item.name, command) || await this.registerMacro(item.name, item.img, command);

    game.user.assignHotbarMacro(macro, slot)
  }

  static roll(itemName) {
    const speaker = ChatMessage.getSpeaker();
    const actor = game.actors.get(speaker.actor) || (speaker.token ? game.actors.tokens[speaker.token] : undefined);

    if (!actor) return Message.warn("MOTW.NoActorSelected");

    const item = actor.items.find(i => i.name === itemName);
    if (item) return item.roll();

    return Message.warn("MOTW.DontPossessItem", {actor: actor.name, item: itemName});
  }

  static find(itemName, command) {
    const macro = game.macros.entities.find(m => (m.name === itemName) && (m.command === command));
    return macro
  }

  static async registerMacro(itemName, itemImage, command) {
    const macro = await Macro.create({
      name: itemName,
      type: "script",
      img: itemImage,
      command: command,
      flags: { "motw.itemMacro": true }
    });

    return macro;
  }
}
