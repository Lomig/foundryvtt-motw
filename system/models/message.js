export class Message {


  static info(message, options) {
    ui.notifications.info(game.i18n.format(message, options));
  }

  static warn(message, options) {
    ui.notifications.warn(game.i18n.format(message, options));
  }
}
